package com.cleverbits.io.spark_examples

/* CSVSparkApp.scala */
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql._
import com.databricks.spark.csv
import org.apache.spark.sql.types._
import org.apache.spark.sql.cassandra
import org.apache.spark.sql.cassandra._
import com.datastax.spark
import com.datastax.spark._
import com.datastax.spark.connector
import com.datastax.spark.connector._
import com.datastax.spark.connector.cql
import com.datastax.spark.connector.cql._
import com.datastax.spark.connector.cql.CassandraConnector
import com.datastax.spark.connector.cql.CassandraConnector._

object Vodafone {
  def main(args: Array[String]) {
    val conf = new SparkConf()
      .setAppName("CSVSparkApp")
      .setMaster("local[*]")
      //.set("spark.cassandra.connection.host", "localhost")
      .set("spark.cassandra.connection.host", "node")
      .set("spark.cassandra.connection.port", "9042")
      .set("spark.debug.maxToStringFields", "100")
      .set("spark.cassandra.input.consistency.level", "LOCAL_ONE")
      .set("spark.cassandra.output.consistency.level", "LOCAL_ONE")
      
    val sc = new SparkContext(conf)
    var sqlContext = new SQLContext(sc)
    //sqlContext.setConf("spark.sql.caseSensitive", "true");

    //val table = sc.cassandraTable("vodafone", "test")
    //val table = sc.cassandraTable("vodafone", "testing")
    val table = sc.cassandraTable("vodafone", "prod")
    println(table.count) //0
    
    //val pathToCSV = "vodafone-test.csv"
    val pathToCSV = "test_with_header.csv"
    val df = sqlContext.read
                    .format("com.databricks.spark.csv")
                    .option("header","true")
                    //.option("timestampFormat", "dd-MM-yyyy HH:mm:ss")
                    .option("timestampFormat", "dd-MM-yyyy HH:mm:ss")
                    //.option("timestampFormat", "dd/MM/yyyy HH:mm") // for vodafone-test.csv
                    .option("inferSchema","true") 
                    .load(pathToCSV)
    println("Starting ...")
    df.rdd.cache()
    //df.rdd.foreach(println)
    println(df.printSchema)
    df.registerTempTable("vodafone")
    //sqlContext.sql("SELECT somedatetime, web FROM vodafone LIMIT 3").collect.foreach(println)
    //sqlContext.sql("SELECT * FROM vodafone LIMIT 3").collect.foreach(println)
    sqlContext.sql(s"SELECT R_P_SIM_ID, START_D_T FROM vodafone LIMIT 3").collect.foreach(println)
    //sqlContext.sql(s"SELECT * FROM vodafone LIMIT 3").collect.foreach(println)

    //val resultset = sqlContext.sql("SELECT * FROM vodafone LIMIT 1000")
    var resultset = sqlContext.sql("SELECT * FROM vodafone")
    
    //resultset.write.cassandraFormat("test", "vodafone").save()
    import org.apache.spark.sql._
    //resultset.write.mode("Append").cassandraFormat("test", "vodafone").save()
    resultset.write.mode("Append").cassandraFormat("prod", "vodafone").save()

//    var limit = 18
//    var count = resultset.count()
//    while(count > 0){
//        val df1 = resultset.limit(limit);
//        resultset.write.mode("Append").cassandraFormat("prod", "vodafone").save()
//        resultset = resultset.except(df1)
//        count = count - limit
//        Thread.sleep(2000)
//    }
    
    println("... Finished")
    
  }
}