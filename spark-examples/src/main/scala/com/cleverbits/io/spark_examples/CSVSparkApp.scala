package com.cleverbits.io.spark_examples

/* CSVSparkApp.scala */
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql._
import com.databricks.spark.csv
import org.apache.spark.sql.types._

object CSVSparkApp {
  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("CSVSparkApp").setMaster("local[*]")
    val sc = new SparkContext(conf)
    var sqlContext = new SQLContext(sc)
    
//    val customSchema = StructType(Array(
//                          StructField("web", StringType, true),   
//                          StructField("somenumber", IntegerType, true),
//                          StructField("someothernumber", IntegerType, true),
//                          StructField("somedatetime", DateType, true)))
    //val pathToCSV = "/root/FL_insurance_sample.csv"
    //val pathToCSV = "cars.csv"
    //val pathToCSV = "vodafone-test.csv"
    val pathToCSV = "test.csv"
    val df = sqlContext.read
                    .format("com.databricks.spark.csv")
                    .option("header","true")
                    .option("timestampFormat", "dd-MM-yyyy HH:mm:ss")
                    .option("inferSchema","true") 
                    //.schema(customSchema)
                    .load(pathToCSV)
    println("Starting ...")
    df.rdd.cache()
    //df.rdd.foreach(println)
    println(df.printSchema)
    df.registerTempTable("vodafone")
    sqlContext.sql("SELECT * FROM vodafone LIMIT 3").collect.foreach(println)
    println("... Finished")
  }
}